<?php

namespace Drupal\mmpp\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserAccessControlHandler;

/**
 * Defines an access control handler for the user entity type.
 */
class MmppAccessHandler extends UserAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\user\UserInterface $entity */
    $access = parent::checkAccess($entity, $operation, $account);
    if (!$access->isAllowed() || $operation !== 'view') {
      return $access;
    }

    // Administrators can view/update/delete all user profiles.
    if ($account->hasPermission('administer users')) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    // Special handling for user.
    if ($entity->id() === $account->id()) {
      return AccessResult::allowed()->cachePerUser();
    }

    // @see \Drupal\user\UserAccessControlHandler::checkAccess()
    // Only allow view access if the account is active.
    if ($account->hasPermission('access user profiles') && $entity->isActive()) {
      if ($entity->get('mmpp')->value == '1') {
        return AccessResult::allowed();
      }
      else {
        return AccessResult::forbidden();
      }
    }

    return $access;
  }

}
