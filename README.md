# Make My Profile Public

Simply enable the module as usual, and go to admin/user/permissions to
configure the permissions 'access user profiles'

- For a full description of the module, visit the
[project page](https://www.drupal.org/project/mmpp).

- Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/mmpp).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- none.


## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Go to permissions » Enable 'view user information' to roles.


## Maintainers

Current maintainers:
- abdulaziz zaid - [abdulaziz zaid](https://www.drupal.org/u/abdulaziz-zaid)
